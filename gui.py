from tkinter import * 
import threading
import time

fenetre = Tk()
def print_lst(lst):
    if (lst == []):
        print([])
        fenetre.update()
    else:
        for pos in range(16):
            Button(fenetre, text=lst[pos], borderwidth=1).grid(row=int(pos/4), column=int(pos%4))
#master.mainloop()

def print_board():
    while 42:
        board = open("board", "r")
        print_lst(board.readline().split())
        board.close()
        fenetre.update()
        time.sleep(1)

fenetre.after(1000, print_board)
fenetre.mainloop()
