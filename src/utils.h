#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>

# define SUCCESS 1
# define ERROR 0

typedef enum e_msg {
    ERR,
    DBG,
    INF,
} t_msg;

void msg_(t_msg type,const char *msg,  ...);
void write_plateau_to_file(int *plateau, int plateau_size);
