#include "ipc.h"
#include "utils.h"

t_direction find_target_direction(t_sys *sys, int team, int position, int prev_position,
                                  t_direction prev_direction, bool first) {

    /* unused because I prefer random moves over the ia doing the same thing over and over */
    (void)prev_direction;
    int t_position = 0;
    for (int i = 0; i < LINES * COLS; i++) {
        if (sys->plateau[i] == team) {
            t_position = i;
            break;
        }
    }
    int t_line = t_position / LINES; 
    int t_col = t_position % COLS;
    int line = position / LINES;
    int col = position % COLS;

    if (first == false) {
        if (prev_position == position)
            return (rand() % 4) + 1;
    }
    
    int v_distance = 0;
    int h_distance = 0;
    if (t_col - col > 0) {
        h_distance = t_col - col;
    }
    else if (t_col - col < 0){
        h_distance = col - t_col;
    }
    else {
        if (t_line - line > 0)
            return DOWN;
        else if (t_line - line < 0)
            return UP;
    }
    if (t_line - line > 0) {
        v_distance = t_line - line;
    }
    else if (t_line - line < 0) {
        v_distance = line - t_line;
    }
    else {
        if (t_col - col > 0)
            return RIGHT;
        else if (t_col - col < 0)
            return LEFT;
    }
    if (v_distance > h_distance) {
        // left or right
        if (t_col - col > 0)
            return RIGHT;
        else if (t_col - col < 0)
            return LEFT;
    }
    else {
        // up or down
        if (t_line - line > 0)
            return DOWN;
        else if (t_line - line < 0)
            return UP;
    }
    return NONE;
}

int is_invalid_team(int team) {
    if (team < 0)
        return 1;

    if (team >= (LINES-1)*(COLS-1))
        return 1;

    return 0;
}

/* never call without a lock on plateau */
int get_target(t_sys *sys, int team) {
    // classer les équipes par nombre
    // copies du plateau sans les cases vides
    int lst1[LINES * COLS];
    int lst2[LINES * COLS];
    int k = 0;
    int target_team = team;
    for (int i = 0; i < LINES * COLS; i++) {
        if (sys->plateau[i] != 0) {
            lst1[k] = sys->plateau[i];
            lst2[k] = sys->plateau[i];
            k++;
        }
    }
    // enlever les doublons
    int size = k;
    int lst2_size = size;
    k = 0;
    for (int i = 0; i < size; i++) {
        for (int j = i + 1; j < size; j++) {
            if (lst1[i] == lst1[j]) {
                for (k = j; k < size - 1; k++) {
                    lst1[k] = lst1[k + 1];  
                }
                size--;
                j--;
            }
        }
    }
    /*
    if (size == 2) {
        for (int i = 0; i < size; i++) {
            if (lst1[i] != team) {
                return lst1[i];
            }
        }
    }
    */
    // create array with team member count
    int lst3[size][2];
    int count = 0;
    for (int i = 0; i < size; i++) {
        count = 0;
        for (int j = 0; j < lst2_size; j++) {
            if (lst1[i] == lst2[j]) {
                count += 1;
            }
        }
        lst3[i][0] = lst1[i];
        lst3[i][1] = count;
    }
    // choose the team to target
    int team_member_count = 0;
    int tm_count = 0;
    for (int i = 0; i < size; i++) {
        if (team == lst3[i][0]) {
            team_member_count = lst3[i][1];            
            break;
        }
    }
    tm_count = team_member_count;
    // choose a team that has less or same team member count
    for (int i = 0; i < size; i++) {
        if (team != lst3[i][0]) {
            if (lst3[i][1] <= team_member_count) {
                team_member_count = lst3[i][1];
                target_team = lst3[i][0];
            }
        }
    }
    // msgsnd to all team members
    char *_sh = (char *)&target_team;
    t_message message = { .msgid=team,.target={}, };
    message.target[0] = _sh[0];
    message.target[1] = _sh[1];
    int ret = 0;
    for (int i = 0; i < tm_count - 1; i++) {
        ret = msgsnd(sys->msg_queue,
                     &message, sizeof(message.target), 0);
        if (ret == -1) {
            printf("sysv snd not working: could not notify members\n");
            printf("I will then target myself\n");
            return team;
        }
    }
    return target_team;
}

int is_surrounded(int position, int team, int *plateau) {
    // clockwise neighboring cells, starting @9 o'clock
    int positions[8] = {
        (position-1),
        (position-1) - COLS,
        (position-1) - COLS + 1,
        (position-1) - COLS + 2,
        (position+1),
        (position+1) + COLS,
        (position+COLS),
        (position+COLS) - 1
    };
    if ((position >= 0) && (position <= COLS - 1)) {
        if (position == 0) {
            positions[0] = -1;
            positions[7] = -1;
        }
        positions[1] = -1;
        positions[2] = -1;
        positions[3] = -1;
        if (position == COLS - 1) {
            positions[4] = -1;
            positions[5] = -1;
        }
    }
    else if ((position >= LINES * COLS - COLS)
             && (position <= LINES * COLS - 1)) {
        if (position == LINES * COLS - COLS) {
            positions[0] = -1;
            positions[1] = -1;        
        }
        positions[5] = -1;
        positions[6] = -1;
        positions[7] = -1;
        if (position == LINES * COLS - 1) {
            positions[3] = -1;
            positions[4] = -1;
        }
    }
    else if (position % COLS == 0) {
        positions[7] = -1;
        positions[0] = -1;
        positions[1] = -1;
    }
    else if (position % COLS == 1) {
        positions[3] = -1;
        positions[4] = -1;
        positions[5] = -1;
    }
    int t = 0;
    int cnt = 0;
    for(int j = 0; j < 8; j++) {
        cnt = 0;
        if (positions[j] != -1) {
            if ((plateau[positions[j]] != 0)
                && (plateau[positions[j]] != team)) {
                t = plateau[positions[j]];
            }
        }
        /* ignore empty cells, and those with our team members */
        if (t == 0) continue;
        for(int i = 0; i < 8; i++) {
            if (positions[i] != -1) {
                if (plateau[positions[i]] == t) {
                    cnt++;
                }
            }
        }
        if (cnt == 2) {
            return 1;
        }
    }
    return 0;
}

int get_next_player(int id, int team, int *plateau) {
    // id, is the starting search index
     for (int i = id; i < LINES * COLS; i++) {
        if (plateau[i] == team) {
            return i;
        }
     }
     return -1;
}

int get_free_places(int *plateau) {
    int count = 0;

    for (int i = 0; i < LINES * COLS; i++) {
        if (plateau[i] == 0) {
            count++;
        }
    }
    return count;
}

int player_place_ops(bool place, int team, int pos, int *plateau) {
    if ((pos >= LINES * COLS) || (pos < 0)) {
        return ERROR;
    }
    if (place) {
        if (plateau[pos]) {
            return ERROR;
        }
        plateau[pos] = team;
    }
    else {
        plateau[pos] = 0;
    }
    
    write_plateau_to_file(plateau, LINES * COLS);
    
    return SUCCESS;
}

static int move_player_ops(int dx, int dy, int pos, int *plateau) {
	// this function won't invalidate the plateau, it will do nothing
	// if the requested move is not allowed. Need to call can_player_move_*()
    // (0,0) is in the top left
    int team = plateau[pos];
    /*
    if (team == 0)
        return;
    */
    int x = pos / LINES;
    int y = pos % COLS;

    int new_pos = 0;
    if (dx != 0) {
        new_pos = (x + dx) * COLS + y;
        if ((x + dx >= 0) && (x + dx < COLS)) {
            if (player_place_ops(1, team, new_pos, plateau)) {
                player_place_ops(0, 0, pos, plateau);
                return new_pos;
            }
        }
    }
    if (dy != 0) {
        new_pos =  x * COLS + (y + dy);
        if ((y + dy >= 0) && (y + dy < LINES)) {
            if (player_place_ops(1, team, new_pos, plateau)) {
                player_place_ops(0, 0, pos, plateau);
                return new_pos;
            }
        }
    }
    return pos;
}

int apply_move(t_direction direction, int position, int *plateau) {
    if (direction == UP) {
        return move_player_ops(0, -1, position, plateau);
    }
    else if (direction == DOWN) {
        return move_player_ops(0, 1, position, plateau);
    }
    else if (direction == LEFT) {
        return move_player_ops(-1, 0, position, plateau);
    }
    else if (direction == RIGHT) {
        return move_player_ops(1, 0, position, plateau);
    }
    return position;
}

int add_player(int team, int *plateau, int *position) {
    if (!plateau) {
        return ERROR;
    }
    /* for now, don't think I need an id */
    int num_places = get_free_places(plateau);
    if (num_places == 0)
        return ERROR;
    int pos = rand() % num_places;
    int count = 0;

    for (int i = 0; i < LINES * COLS; i++) {
        if (plateau[i] == 0) {
            if (count == pos) {
                if (player_place_ops(1, team, i, plateau)) {
                    *position = i;
                    return SUCCESS;
                }
            }
            count++;
        }
    }
    return ERROR;
}

int delete_player(int pos, int *plateau) {
    if (!plateau) {
        return ERROR;
    }

    if (pos >= LINES * COLS)
        return ERROR;
    if (player_place_ops(0, 0, pos, plateau))
        return SUCCESS;
    return ERROR;
}

int init_board (int **plateau) {
    srand(time(0));
    *plateau = malloc(sizeof(int) * LINES * COLS);
    if (*plateau == NULL) {
        msg_(ERR, "malloc() failed, could not init board!");
        return ERROR;
    }
    memset(*plateau, 0, sizeof(int) * LINES * COLS);
    return SUCCESS;
}

int destroy_board (int **plateau) {
    if (*plateau) {
        free(*plateau);
        *plateau = NULL;
        return SUCCESS;
    }
    return ERROR;
}
