#include "ipc.h"
#include <string.h>
#include <stdio.h>

int init_sys (t_sys *sys) {
    memset(sys->plateau, 0, LINES * COLS * sizeof(int));
    if (sem_init(&sys->s_plock, 1, 1)) {
        return 1;
    }
    if (sem_init(&sys->s_run, 1, 0)) {
        return 1;
    }
    return 0;
}

void start_simulation (t_sys *sys) {
    printf("Starting simulation\n");
    // sem_post(&sys->s_plock);
    sem_post(&sys->s_run);
    key_t msg_key = ftok("msgs", 42 + sys->num);
    sys->msg_queue = msgget(msg_key, 0666 | IPC_CREAT);
}
/* OLD
void setup_new_team(t_sys *sys, int team) {
    sys->bags[team - 1] = (t_tbag){ .team = team, .count= 1 };
    sem_init(&sys->bags[team - 1].start, 1, 0);
    sem_init(&sys->bags[team - 1].s_target, 1, 0);
    sem_init(&sys->bags[team - 1].s_running, 1, 0);
    key_t msg_key = ftok("msgs", 42 + 1 + sys->num);
    sys->bags[team - 1].msg_queue = msgget(msg_key, 0666 | IPC_CREAT);
    sys->bags[team - 1].target = 0;
    sys->bags[team - 1].running = true;
    sys->team_num += 1;
}
*/
