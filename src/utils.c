#include "utils.h"
#include <stdarg.h>

void msg_(t_msg type, const char *msg, ...) {
    // FIXME: faire une macro
    va_list argList;
    va_start(argList, msg);
    if (type == ERR) {
        printf("error : ");
        vprintf(msg, argList);
    }
    else if (type == DBG) {
        printf("dbg : ");
        vprintf(msg, argList);
    }
    else if (type == INF) {
        printf("info : ");
        vprintf(msg, argList);
    }
    va_end(argList);
    printf("\n");


}

void write_plateau_to_file(int *plateau, int plateau_size) {
    FILE *stream = fopen("./board", "r+");
    if (!stream) {
        msg_(ERR, "write_to_file: could not open file");
        return;
    }
    for (int i = 0; i < plateau_size; i++) {
        fprintf(stream, "%d ", plateau[i]);
    }
    fprintf(stream, "\n");
    int ret = fclose(stream);
    if (ret != 0) {
        msg_(ERR, "write_to_file: could not close file");
    }
}
