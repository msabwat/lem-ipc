#include "ipc.h"
#include "src/utils.h"
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
/*
  usage:

  ./lemipc <team-number>
  must be valid, ub if you enter wild numbers
  ./lemipc -1
  something went wrong and you want to destroy ipc ressources
  ./lemipc
  Start the simulation

  for the gui, any program could read the "board" file, but it should agree on
  LINES/COLS defines in gameboard.h
*/
bool term = false;
void sig_handler(int signum) {
    printf("received signal %d! terminating...\n", signum);
    term = true;
}

int main(int ac, char **av) {
    // setup per process seed
    srand(getpid());
    signal(SIGINT, sig_handler);
    // signal(SIGQUIT, sig_handler);
    // signal(SIGTERM, sig_handler);
    // signal(SIGKILL, sig_handler);
    /* pendant la correction on demande de gerer SIGINT mais pas les autres */
    t_message message;
    key_t key = ftok("lemipc", 42);
    if (key == -1) {
        printf("error, could not get ipc key\n");
        return 1;
    }
    if (ac > 2) {
        printf("wrong options!\n");
        printf("usage: ./lemipc <team>\n");
        return 1;
    }

    int shmid = shmget(key, sizeof(t_sys), IPC_CREAT | 0666);
    t_sys *sys = (t_sys *) shmat(shmid, (void*)0, 0);
    if (sys == (void *)-1) {
        printf("error: %d", errno);
        exit(1);
    }
    if (sys->num == 0) {
        if (init_sys(sys)) {
            goto destroy;
        }
    }
    if (ac == 1) {
        start_simulation(sys);
        return 0;
    }
    int team = atoi(av[1]);
    if (team == -1) {
        goto destroy;
    }
    if (is_invalid_team(team)) {
        printf("invalid team number! %d\n", team);
        return 1;
    }
    
    bool exists = false;
    int position = 0;
    sem_wait(&sys->s_plock);
    // are we going to create a team or not ?
    for (int i = 0; i < LINES * COLS; i++) {
        if (sys->plateau[i] == team) {
            exists = true;
            break;
        }
    }
    if (!add_player(team, sys->plateau, &position)) {
        printf("error could not add player from team %d\n", team);
        sem_post(&sys->s_plock);
        goto destroy;
    }
    printf("added team %d in position %d\n", team, position);
    sys->num += 1;
    sem_post(&sys->s_plock);
    /* 
       Here, we need to block in order to run this code once the plateau
       is completely setup.

       get_target should not be called if we don't have all the players
       placed in the plateau.
    */
    sem_wait(&sys->s_run);
    // releasing the lock right after for other agents waiting on it.
    sem_post(&sys->s_run);
    int target = team;
    int ret = 0;
    if (exists == false) {
        printf("creating a new team %d\n", team);
        /*
          default is 0
          if the player targets itself it will move randomly.          
        */
        sem_wait(&sys->s_plock);
        target = get_target(sys, team);
        sem_post(&sys->s_plock);
    }
    else {
        ret = msgrcv(sys->msg_queue, &message, sizeof(message.target), team,
                     0);
        if(ret == -1) {
            printf("sysv rcv not working: could not get target\n");
            printf("aborting, I cannot continue, trying to leave cleanly\n");
            term = 1;
        }
        else {
            int tmp = 0;
            tmp = tmp | message.target[1];
            tmp = tmp << 8;
            tmp = tmp | message.target[0];
            target = tmp;
            printf("got new target : %d\n", target);
        }
    }
    printf("created team:%d position:%d targetting:%d\n",
           team, position, target);    
    int prev_position = 0;
    bool first = true;
    t_direction prev_direction = NONE;
    for(;;) {
        sem_wait(&sys->s_plock);
        // 1. find target direction
        t_direction direction = find_target_direction(sys,
                                                      target,
                                                      position,
                                                      prev_position,
                                                      prev_direction,
                                                      first);
        if (first == true)
            first = false;
        // 2. move target in the right direction
        if (direction != NONE) {
            int new = apply_move(direction, position, sys->plateau);
            position = new;
            prev_position = position;
            prev_direction = direction;
            printf("moving to position %d\n", position);
        }
		// 3. check if I should die
        if ((is_surrounded(position, team, sys->plateau)) ||
            (term == true)) {
            player_place_ops(0, 0, position, sys->plateau);
            printf("player from team %d is dying\n", team);
            sem_post(&sys->s_plock);
            break;
        }
        // 4. check me and my team are the last ones standing.
        bool won = true;
        for (int i = 0; i < LINES * COLS; i++) {
            if ((sys->plateau[i] != team) && (sys->plateau[i] != 0)) {
                won = false;
                break;
            }
        }
        if (won) {
            sem_post(&sys->s_plock);
            printf("team %d has won!!\n", team);
            goto destroy;
        }
        sem_post(&sys->s_plock);
        sleep(1);
    }
    sem_wait(&sys->s_plock);
    sys->num -= 1;
    if (sys->num == 0) {
        sem_post(&sys->s_plock);
        goto destroy;
    }
    sem_post(&sys->s_plock);
    return 0;
destroy:
     if (sys) {
        // reset plateau to 0 for the gui
        for (int i = 0; i < LINES * COLS; i++) {
            sys->plateau[i] = 0;
        }
        write_plateau_to_file(sys->plateau, LINES*COLS);
        msgctl(sys->msg_queue, IPC_RMID, NULL);
        sem_destroy(&sys->s_plock);
        sem_destroy(&sys->s_run);
        shmctl(shmid, IPC_RMID, NULL);
        shmdt(sys);
    }
    return 0;
}
