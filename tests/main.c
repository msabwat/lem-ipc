#include <assert.h>
#include <unistd.h>
#include "utils.h"
#include "ipc.h"
/*
    pour afficher le plateau:
    watch -t -n 0.1 cat board
*/
int main (int ac, char **av) {
    (void)ac, (void)av;
    int *plateau = NULL;
    int free_places = 0;
    int position = 0;
    int ret = 0;(void)ret;
    
    msg_(INF, "Creating board with lines:%d and cols:%d", LINES, COLS);
    if (!init_board(&plateau)) {
        msg_(ERR, "Could not create board");
        goto clean_exit;
    }
    

    // ###########################################
    msg_(INF, "Starting player creation test: 1");
    msg_(INF, "Creating 1 player in team %d", 1);
    add_player(1, plateau);
    sleep(2);
    free_places = get_free_places(plateau);
    assert (free_places == (LINES * COLS) - 1);
    position = get_next_player(0, 1, plateau);
    msg_(INF, "Player in team %d, is in position %d", 1, position);
    assert(position != -1);
    msg_(INF, "Player in position %d, from team %d died", position, 1);
    delete_player(position, plateau);
    free_places = get_free_places(plateau);
    position = get_next_player(0, 1, plateau);
    assert(free_places == LINES * COLS);
    assert(position == -1);


    // ###########################################
    msg_(INF, "Starting player creation test: 2");

    msg_(INF, "Creating %d players", 10);
    for (int i = 0; i < 10; i++) {
        position = (rand() % 4) + 1; // bad name
        add_player(position, plateau);
        msg_(INF, "Created player in team %d", position);
    }
    free_places = get_free_places(plateau);
    assert(free_places == (LINES * COLS) - 10);

    sleep(2);

    for (int team = 1; team < 5; team++) {
        int j = 0;
        position = get_next_player(0, team, plateau);
        j++;
        while (position != -1) {
            msg_(INF, "deleting player from team %d in position %d", team, position);
            delete_player(position, plateau);
            position = get_next_player(position + 1, team, plateau);
            j++;
        }
    }
    free_places = get_free_places(plateau);
    assert(free_places == (LINES * COLS));
    

    // ##############################################
    msg_(INF, "Starting player creation test: 3");
    
    msg_(INF, "Overflowing the board with %d players", LINES * COLS);
    for (int i = 0; i < LINES * COLS; i++) {
        position = (rand() % 4) + 1; // bad name
        add_player(position, plateau);
        msg_(INF, "Created player in team %d", position);
    }
    
    sleep(2);

    msg_(INF, "Adding one player, should fail (assert)");
    ret = add_player(position, plateau);
    assert(ret == ERROR);

    msg_(INF, "Any operation on an invalid plateau should fail (assert)");
    ret = add_player(1, NULL);
    assert(ret == ERROR);
    ret = delete_player(0, NULL);
    assert(ret == ERROR);

    // other operations will arrive later (no need for a null check)
    
    msg_(INF, "Removing player from invalid position should fail (assert)");
    ret = delete_player(42, plateau);
    assert(ret == ERROR);

    msg_(INF, "Removing all players");
    for (int i = 0; i < LINES * COLS; i++) {
        msg_(INF, "Removing player in %d from team %d", i, plateau[i]);
        delete_player(i, plateau);
    }
    free_places = get_free_places(plateau);
    assert(free_places == LINES * COLS);

clean_exit:
    if (!destroy_board(&plateau)) {
        msg_(ERR, "Could not destroy board");
        return 1;
    }
    return 0;
}
