NAME			= lemipc

FLAGS 			= -g3 -Wall -Wextra -Werror -pthread -fsanitize=address,undefined

CC				= cc

INC				= -I. -Isrc 

SRC_NAME		= gameboard.c utils.c ipc.c

OBJ_NAME		= $(SRC_NAME:.c=.o)

SRC_PATH		= src

OBJ_PATH		= .obj

SRC=$(addprefix $(SRC_PATH)/,$(SRC_NAME))

OBJS=$(addprefix $(OBJ_PATH)/,$(OBJ_NAME)) 

TEST=

all: makedir $(NAME)

makedir:
	@mkdir -p .obj

test: FLAGS += -g3 -fsanitize=address,undefined
test: TEST = test
test: makedir $(NAME)
	$(CC) $(FLAGS) tests/main.c $(OBJS) $(INC) -o tests_$(NAME)

$(NAME): $(OBJ_PATH)/main.o $(OBJS) ipc.h
	$(CC) $(FLAGS) $(OBJ_PATH)/main.o  $(OBJS) $(INC) -lpthread -lrt -o $(NAME)

$(OBJ_PATH)/main.o: main.c
	$(CC) $(FLAGS)  $(INC) -c main.c -o $(OBJ_PATH)/main.o

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	$(CC) $(FLAGS)  $(INC) -c $< -o $@

clean:
	rm -fr $(OBJ_PATH)

fclean: clean
	rm -fr $(NAME)
	rm -fr tests_$(NAME)
