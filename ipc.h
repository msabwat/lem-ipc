#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/msg.h>
#include <semaphore.h>
#include <assert.h>
#include <unistd.h>
#include <stdbool.h>
#include <errno.h>

#define LINES 4
#if LINES < 4
#error "LINES too small"
#endif

#define COLS 4
#if COLS < 4
#error "COLS too small"
#endif

#if LINES * COLS > 1000
#error "plateau is way too big, please choose a reasonnable value (L*C < 10000)"
#endif

typedef enum e_direction {
    NONE,
    LEFT,
    RIGHT,
    UP,
    DOWN
} t_direction;

typedef struct s_message {
    long msgid;
    char target[2];
} t_message;

typedef struct s_sys {
    // game board
    int plateau[LINES*COLS];
    // semaphore for the plateau lock
    sem_t s_plock;
    // semaphore for the simulation
    sem_t s_run;
    int msg_queue;
    // number of players
    int num;
} t_sys;

int get_free_places(int *plateau);
int get_next_player(int id, int team, int *plateau);
int add_player(int team, int *plateau, int *position);
int delete_player(int pos, int *plateau);
int init_board (int **plateau);
int destroy_board (int **plateau);
int apply_move(t_direction direction, int position, int *plateau);
int is_invalid_team(int team);
int init_sys (t_sys *sys);
void start_simulation (t_sys *sys);
void setup_new_team(t_sys *sys, int team);

int player_place_ops(bool place, int team, int pos, int *plateau);
t_direction find_target_direction(t_sys *sys, int team, int position, int prev_position,
                                  t_direction prev_direction, bool first);
int get_target(t_sys *sys, int team);
int is_surrounded(int position, int team, int *plateau);
